package fit.and.solution.featueres.home.presentation

import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.lifecycle.viewmodel.compose.viewModel
import fit.and.solution.core.domain.GPSLocation
import fit.and.solution.core.presentation.ui.components.NotActiveSearchBar
import fit.and.solution.featueres.places.presentation.components.PlacePreview
import fit.and.solution.featueres.places.presentation.components.VisitingPlace
import kotlin.math.log


@Composable
fun Home(
 paddingValues: PaddingValues,
 homeScreenViewModel: HomeScreenViewModel = viewModel(),
 navigateToPlaceDetail: (id: String) -> Unit,
 navigateToSearchScreen: () -> Unit) {
	val homeScreenState by homeScreenViewModel.screenState.collectAsStateWithLifecycle()
	Log.d(
	 "UPDATE",
	 "recomp")
	Home(
	 paddingValues = paddingValues,
	 screenState = homeScreenState,
	 updatePosition = { homeScreenViewModel.updatePosition() },
	 addVisited = {
		 homeScreenViewModel.addVisited(it); Log.d(
		"TEST",
		(homeScreenState as HomeScreenState.Loaded).visiting.size.toString())
	 },
	 navigateToPlaceDetail = navigateToPlaceDetail,
	 navigateToSearchScreen = navigateToSearchScreen)
}

@Composable
private fun Home(
 paddingValues: PaddingValues,
 screenState: HomeScreenState,
 updatePosition: (/*GPSLocation*/) -> Unit,
 addVisited: (String) -> Unit,
 navigateToPlaceDetail: (String) -> Unit,
 navigateToSearchScreen: () -> Unit) {
	LazyColumn(
	 modifier = Modifier.fillMaxWidth(),
	 contentPadding = paddingValues) {
		item {
			NotActiveSearchBar { navigateToSearchScreen() }
			when (screenState) {
				is HomeScreenState.Loading -> {
					Text(text = "Loading")
				}

				is HomeScreenState.Loaded -> {
					if (screenState.visiting.isNotEmpty()) Column() {
						Text(
						 modifier = Modifier
							 .fillMaxWidth()
							 .background(MaterialTheme.colorScheme.surfaceVariant)
							 .padding(10.dp),
						 text = "To add",
						 fontSize = 30.sp)
						for (visiting in screenState.visiting) {
							VisitingPlace(place = visiting,
							 color = MaterialTheme.colorScheme.surfaceVariant,
							 navToDetail = { navigateToPlaceDetail(visiting.id) },
							 addVisited = { addVisited(visiting.id) })
						}
					}
					PlacePreview(
					 headLine = "Recommended",
					 place = screenState.recommendedPlace) {
						navigateToPlaceDetail(screenState.recommendedPlace.id)
					}
					PlacePreview(
					 headLine = "Hot Place",
					 place = screenState.hotPlace) {
						navigateToPlaceDetail(screenState.hotPlace.id)
					}
					updatePosition()
				}
			}
		}
	}
}


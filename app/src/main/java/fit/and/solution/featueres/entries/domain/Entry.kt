package fit.and.solution.featueres.entries.domain

import fit.and.solution.R
import fit.and.solution.core.domain.Place
import fit.and.solution.core.presentation.ui.image.Image
import java.time.LocalDate

data class Entry(
 val entryId: String,
 val place: Place,
 val text: String,
 val image: Image,
 val rating: Int,
 val visitDate: LocalDate,
 val weather: Weather,
 val transportation: Transportation)

enum class Weather(
 val nameResource: Int,
 val iconRes: Int) {
	SUNNY(
	 nameResource = R.string.sunny,
	 iconRes = R.drawable.sunny),
	CLOUDY(
	 nameResource = R.string.cloudy,
	 iconRes = R.drawable.cloudy),
	RAINY(
	 nameResource = R.string.rainy,
	 iconRes = R.drawable.rain),
}

enum class Transportation(
 val nameResource: Int,
 val iconRes: Int) {
	WALK(
	 nameResource = R.string.walk,
	 iconRes = R.drawable.walk),
	CAR(
	 nameResource = R.string.car,
	 iconRes = R.drawable.car),
	BUS(
	 nameResource = R.string.bus,
	 iconRes = R.drawable.bus),
	SHIP(
	 nameResource = R.string.ship,
	 iconRes = R.drawable.ship),
	TRAIN(
	 nameResource = R.string.train,
	 iconRes = R.drawable.train),
	BICYCLE(
	 nameResource = R.string.bicycle,
	 iconRes = R.drawable.bicycle),
	MOTORCYCLE(
	 nameResource = R.string.motorcycle,
	 iconRes = R.drawable.motorbike),
}
package fit.and.solution.featueres.places.presentation.search

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.lifecycle.viewmodel.compose.viewModel
import fit.and.solution.core.presentation.ui.components.ActiveSearchBar
import fit.and.solution.featueres.places.presentation.components.PlacePreview


@Composable
fun SearchScreen(
 paddingValues: PaddingValues,
 searchScreenViewModel: SearchScreenViewModel = viewModel(),
 navigateBack: () -> Unit,
 navigateToPlaceDetail: (id: String) -> Unit) {
	val searchScreenState by searchScreenViewModel.searchState.collectAsState()
	Column(
	 modifier = Modifier) {
		ActiveSearchBar(
		 onValueChange = {
			 searchScreenViewModel.updateSearchString(it)
		 },
		 onCancel = {
			 if (searchScreenState.searchString == "") navigateBack()
			 else searchScreenViewModel.updateSearchString("")
		 },
		 searchText = searchScreenState.searchString)
		LazyColumn (contentPadding = paddingValues){
			items(searchScreenState.searchResult) {
				PlacePreview(place = it) { navigateToPlaceDetail(it.id) }
			}
		}
	}
}
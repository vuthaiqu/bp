package fit.and.solution.featueres.entries.presentation.list

import VisitedPlace
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fit.and.solution.core.domain.Place
import fit.and.solution.featueres.entries.data.UserEntryRepository
import fit.and.solution.featueres.entries.domain.Entry
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch


sealed interface EntriesScreenState {
	data object Loading : EntriesScreenState
	data class Loaded(
	 val entries: List<Entry>,
	 val visited: List<VisitedPlace>) : EntriesScreenState
}

class EntriesViewModel(
 entryRepository: UserEntryRepository = UserEntryRepository()) : ViewModel() {
	private val _screenState = MutableStateFlow<EntriesScreenState>(EntriesScreenState.Loading)
	val screenState get() = _screenState.asStateFlow()

	init {
		viewModelScope.launch {
			_screenState.value = EntriesScreenState.Loaded(
			 entryRepository.getEntries(),
			 entryRepository.getVisited())
		}
	}
}
package fit.and.solution.featueres.places.data

import fit.and.solution.core.data.Data
import fit.and.solution.core.domain.Place

class PlacesRepository {
  fun getPlaces () : List <Place> = Data.places
  fun getPlace (id: String) : Place? = Data.places.find { it.id == id }
}
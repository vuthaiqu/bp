package fit.and.solution.featueres.entries.data

import VisitedPlace
import fit.and.solution.core.data.Data
import fit.and.solution.core.domain.Place
import fit.and.solution.featueres.entries.domain.Entry

class UserEntryRepository () {
	fun getEntries() : List<Entry> = Data.entries

	fun getEntry(id : String) : Entry? = Data.entries.find { it.entryId == id }

	fun getVisited() : List<VisitedPlace> = Data.visited
}
package fit.and.solution.featueres.entries.presentation.components

import VisitedPlace
import android.widget.NumberPicker.OnValueChangeListener
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import fit.and.solution.R
import java.time.format.DateTimeFormatter

@Composable
fun VisitedPlacePreview(
 visited: VisitedPlace,
 onClick: () -> Unit,
) {
	Column(modifier = Modifier
		.padding(bottom = 10.dp)
		.background(MaterialTheme.colorScheme.surfaceVariant)
		.clickable { onClick() }) {
		Divider(
		 Modifier.fillMaxWidth(),
		 thickness = 0.5.dp,
		 color = MaterialTheme.colorScheme.onSurface)
		Column(
		 modifier = Modifier
			 .padding(10.dp)) {
			Row(
			 modifier = Modifier
				 .fillMaxWidth()
				 .height(180.dp),
			 horizontalArrangement = Arrangement.Start) {
				visited.place.image.ComposeImage(
				 contentScale = ContentScale.Crop,
				 modifier = Modifier
					 .weight(5f)
					 .padding(end = 4.dp)
					 .border(
					  width = 1.dp,
					  color = MaterialTheme.colorScheme.onSurface,
					  shape = RoundedCornerShape(5))
					 .clip(RoundedCornerShape(5)))
				Column(
				 modifier = Modifier
					 .weight(3f)
					 .padding(end = 4.dp)) {
					Text(text = visited.place.name)
					Text(text = stringResource(id = R.string.visited) + " "
					 + visited.date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")))
					Spacer(modifier = Modifier.weight(1f))
					Row ( modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.End) {
						Icon(
						 modifier = Modifier.size(20.dp),
						 painter = painterResource(id = R.drawable.add_plus),
						 contentDescription = R.drawable.add_plus.toString())
					}
				}
			}
		}

		Divider(
		 Modifier.fillMaxWidth(),
		 thickness = 0.5.dp,
		 color = MaterialTheme.colorScheme.onSurface)
	}

}
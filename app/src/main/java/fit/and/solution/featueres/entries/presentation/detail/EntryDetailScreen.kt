package fit.and.solution.featueres.entries.presentation.detail

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.viewmodel.compose.viewModel
import fit.and.solution.R
import fit.and.solution.core.presentation.ui.components.DetailTemplate
import fit.and.solution.featueres.entries.domain.Entry
import java.time.format.DateTimeFormatter

@Composable
fun EntryDetailScreen(
 entryDetailViewModel: EntryDetailViewModel = viewModel {
	 EntryDetailViewModel(this.createSavedStateHandle())
 },
 paddingValues: PaddingValues,
 navigateUp: () -> Unit) {
	val screenState by entryDetailViewModel.screenState.collectAsState()
	EntryDetailScreen(
	 screenState = screenState,
	 navigateUp = navigateUp,
	 paddingValues = paddingValues)
}

@Composable
private fun EntryDetailScreen(
 screenState: EntryDetailScreenState,
 paddingValues: PaddingValues,
 navigateUp: () -> Unit) {
	DetailTemplate(
	 paddingValues = paddingValues,
	 title = stringResource(R.string.entry_detail),
	 navigateUp = navigateUp) {
		when (screenState) {
			is EntryDetailScreenState.Loading -> {}
			is EntryDetailScreenState.Loaded -> ScreenContent(entry = screenState.entry)
		}
	}
}

@Composable
private fun ScreenContent(
 entry: Entry) {
	Text(
	 modifier = Modifier
		 .padding(horizontal = 10.dp)
		 .padding(bottom = 10.dp),
	 text = entry.place.name,
	 fontWeight = FontWeight.Bold,
	 fontSize = 25.sp)

	entry.image.ComposeImage(
	 modifier = Modifier
		 .padding(horizontal = 10.dp)
		 .padding(bottom = 10.dp)
		 .height(250.dp)
		 .border(
		  width = 1.dp,
		  color = MaterialTheme.colorScheme.onSurface,
		  shape = RoundedCornerShape(7))
		 .clip(RoundedCornerShape(7)),
	 contentScale = ContentScale.Crop)

	Row(
	 modifier = Modifier
		 .padding(horizontal = 10.dp)
		 .padding(bottom = 10.dp),
	 verticalAlignment = Alignment.CenterVertically) {
		Row(
		 verticalAlignment = Alignment.CenterVertically,
		 modifier = Modifier.weight(5f)) {
			for (i: Int in 1..5) {
				Icon(
				 modifier = Modifier
					 .size(28.dp)
					 .padding(4.dp),
				 painter = painterResource(id = R.drawable.star),
				 contentDescription = R.drawable.star.toString(),
				 tint = if (entry.place.rating >= i) MaterialTheme.colorScheme.secondary else MaterialTheme.colorScheme.onSurface)
			}

		}
		Row(
		 verticalAlignment = Alignment.CenterVertically,
		 modifier = Modifier.weight(5f),
		 horizontalArrangement = Arrangement.End) {
			Icon(
			 modifier = Modifier
				 .padding(end = 5.dp)
				 .size(28.dp),
			 painter = painterResource(id = entry.transportation.iconRes),
			 contentDescription = stringResource(id = entry.transportation.nameResource),
			)
			Icon(
			 modifier = Modifier.size(28.dp),
			 painter = painterResource(id = entry.weather.iconRes),
			 contentDescription = stringResource(id = entry.weather.nameResource))
		}
	}
	Text(
	 modifier = Modifier.padding(horizontal = 10.dp),
	 text = entry.text)
	Text(
	 modifier = Modifier.padding(horizontal = 10.dp),
	 text = stringResource(R.string.visited) + " " + entry.visitDate.format(
		DateTimeFormatter.ofPattern("dd.MM.yyyy")))

}
package fit.and.solution.featueres.entries.presentation.list

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import fit.and.solution.R
import fit.and.solution.core.presentation.ui.components.ScaffoldTopBar
import fit.and.solution.featueres.entries.presentation.components.EntryPreview
import fit.and.solution.featueres.entries.presentation.components.VisitedPlacePreview

@Composable
fun Entries(
 entriesViewModel: EntriesViewModel = viewModel(),
 paddingValues: PaddingValues,
 navigateToEntryDetail: (id: String) -> Unit,
) {
	val screenState by entriesViewModel.screenState.collectAsState()
	Entries(
	 screenState = screenState,
	 paddingValues = paddingValues,
	 navigateToEntryDetail = navigateToEntryDetail)
}

@Composable
private fun Entries(
 screenState: EntriesScreenState,
 paddingValues: PaddingValues,
 navigateToEntryDetail: (id: String) -> Unit,
) {
	ScaffoldTopBar(title = stringResource(R.string.your_journey)) { topBarPadding ->
		when (screenState) {
			is EntriesScreenState.Loading -> {}
			is EntriesScreenState.Loaded -> {
				LazyColumn(
				 modifier = Modifier.padding(topBarPadding),
				 contentPadding = paddingValues) {
					item {
						if (screenState.visited.isNotEmpty()) Column {
							Text(
							 modifier = Modifier
								 .fillMaxWidth()
								 .background(MaterialTheme.colorScheme.surfaceVariant)
								 .padding(horizontal = 10.dp),
							 text = stringResource(
								R.string.visited_to_be_added),
							 fontSize = 20.sp)
							for (it in screenState.visited) VisitedPlacePreview(visited = it) { }
						}
					}
					item {
						for (it in screenState.entries) EntryPreview(entry = it) { navigateToEntryDetail(it.entryId) }
					}
				}
			}
		}
	}
}



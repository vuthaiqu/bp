package fit.and.solution.featueres.home.presentation

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fit.and.solution.core.domain.GPSLocation
import fit.and.solution.core.domain.Place
import fit.and.solution.featueres.home.data.HomeRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import okhttp3.internal.wait

sealed interface HomeScreenState {

	data object Loading : HomeScreenState

	data class Loaded(
	 val recommendedPlace: Place,
	 val hotPlace: Place,
	 val visiting: List<Place> = emptyList()) : HomeScreenState {}
}

//No DI and especially Coil in my watch
class HomeScreenViewModel(
 private val homeRepository: HomeRepository = HomeRepository()) : ViewModel() {
	private val _screenState = MutableStateFlow<HomeScreenState>(HomeScreenState.Loading)
	val screenState get() = _screenState.asStateFlow()

	init {
		viewModelScope.launch {
			_screenState.value = HomeScreenState.Loaded(
			 homeRepository.getRecommended(),
			 homeRepository.getHotSpot())
		}
	}

	fun updatePosition( /*position: GPSLocation*/) {
		viewModelScope.launch {
			_screenState.update { HomeScreenState.Loading }

			_screenState.update {
				HomeScreenState.Loaded(
				 homeRepository.getRecommended(),
				 homeRepository.getHotSpot(),
				 homeRepository.getVisiting())
			}
		}
	}

	fun addVisited(id: String) {
		_screenState.update { HomeScreenState.Loading }
		viewModelScope.launch {
			delay(1) //why it doesnt work without this
			homeRepository.updateVisited(id)
			Log.d(
			 "UPDATE",
			 homeRepository.getVisiting().size.toString())
			val visiting = homeRepository.getVisiting()
			_screenState.update {
				HomeScreenState.Loaded(
				 homeRepository.getRecommended(),
				 homeRepository.getHotSpot(),
				 visiting)
			}
		}
	}
}
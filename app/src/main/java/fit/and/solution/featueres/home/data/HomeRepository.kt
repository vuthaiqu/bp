package fit.and.solution.featueres.home.data

import VisitedPlace
import fit.and.solution.core.data.Data
import fit.and.solution.core.domain.GPSLocation
import fit.and.solution.core.domain.Place
import java.time.LocalDate

class HomeRepository {
	suspend fun getRecommended(): Place = Data.places[0]
	suspend fun getHotSpot(): Place = Data.places[1]
	suspend fun getVisiting( /* location: GPSLocation */): List<Place> = Data.visiting

	suspend fun updateVisited(id: String) {
		//no optimization
		val removedItem: Place? = Data.visiting.find { place -> place.id == id }
		if (removedItem != null) {
			Data.visited.add(VisitedPlace(removedItem,LocalDate.now()))
			Data.visiting.remove(removedItem)
		}
	}
}
package fit.and.solution.featueres.places.presentation.detail

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.createSavedStateHandle
import androidx.lifecycle.viewmodel.compose.viewModel
import fit.and.solution.R
import fit.and.solution.core.domain.Place
import fit.and.solution.core.presentation.ui.components.DetailTemplate
import fit.and.solution.core.presentation.ui.components.FormatNumber

@Composable
fun PlaceDetailScreen(
 paddingValues: PaddingValues,
 placeDetailViewModel: PlaceDetailViewModel = viewModel {
	 PlaceDetailViewModel(this.createSavedStateHandle())
 },
 navigateUp: () -> Unit) {
	val screenState by placeDetailViewModel.screenState.collectAsState()
	PlaceDetailScreen(
	 paddingValues,
	 screenState,
	 navigateUp)
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
private fun PlaceDetailScreen(
 paddingValues: PaddingValues,
 screenState: PlaceDetailScreenState,
 navigateUp: () -> Unit) {
	DetailTemplate(
	 paddingValues = paddingValues,
	 title = stringResource(id = R.string.place_detail),
	 navigateUp = navigateUp) {
		when (screenState) {
			is PlaceDetailScreenState.Loading -> {}
			is PlaceDetailScreenState.Loaded -> {
				ScreenContent(place = screenState.place)
			}
		}
	}
}

@Composable
private fun ScreenContent(
 place: Place) {
	Text(
	 modifier = Modifier
		 .padding(horizontal = 10.dp)
		 .padding(bottom = 10.dp),
	 text = place.name,
	 fontWeight = FontWeight.Bold,
	 fontSize = 25.sp)

	place.image.ComposeImage(
	 modifier = Modifier
		 .padding(horizontal = 10.dp)
		 .padding(bottom = 10.dp)
		 .height(250.dp)
		 .border(
		  width = 1.dp,
		  color = MaterialTheme.colorScheme.onSurface,
		  shape = RoundedCornerShape(7))
		 .clip(RoundedCornerShape(7)),
	 contentScale = ContentScale.Crop)

	Row(
	 modifier = Modifier
		 .padding(horizontal = 10.dp)
		 .padding(bottom = 10.dp),
	 verticalAlignment = Alignment.CenterVertically) {
		Row(
		 verticalAlignment = Alignment.CenterVertically,
		 modifier = Modifier.weight(5f)) {
			FormatNumber(
			 fontSize = 25.sp,
			 number = place.visits)
			Spacer(modifier = Modifier.weight(1f))
			Text(
			 text = "${place.rating}",
			 fontSize = 25.sp)
			Icon(
			 painter = painterResource(id = R.drawable.star),
			 tint = MaterialTheme.colorScheme.onSurface,
			 contentDescription = "stars",
			 modifier = Modifier.size(25.dp))
		}
		Row(
		 verticalAlignment = Alignment.CenterVertically,
		 modifier = Modifier.weight(5f),
		 horizontalArrangement = Arrangement.End) {
			Icon(
			 modifier = Modifier
				 .padding(end = 5.dp)
				 .size(25.dp),
			 painter = painterResource(id = place.category.iconRes),
			 contentDescription = stringResource(id = place.category.nameResource),
			)
			Icon(
			 modifier = Modifier.size(25.dp),
			 painter = painterResource(id = place.recommendedSeason.iconRes),
			 contentDescription = stringResource(id = place.recommendedSeason.nameRes))
		}
	}
	Text(
	 modifier = Modifier.padding(horizontal = 10.dp),
	 text = place.description)
}
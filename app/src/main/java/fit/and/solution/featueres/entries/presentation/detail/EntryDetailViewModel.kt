package fit.and.solution.featueres.entries.presentation.detail

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fit.and.solution.core.presentation.Screens
import fit.and.solution.featueres.entries.data.UserEntryRepository
import fit.and.solution.featueres.entries.domain.Entry
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

sealed interface EntryDetailScreenState {
	data object Loading : EntryDetailScreenState
	data class Loaded (val entry: Entry) : EntryDetailScreenState
}

class EntryDetailViewModel(
 private val savedStateHandle: SavedStateHandle,
 private val entryRepository: UserEntryRepository = UserEntryRepository()) : ViewModel() {
	private val _screenState = MutableStateFlow<EntryDetailScreenState>(EntryDetailScreenState.Loading)
	val screenState get() = _screenState.asStateFlow()

	init {
		viewModelScope.launch {

			val entryId: String = savedStateHandle[Screens.EntryDetail.ID] ?: error("entry id missing")
			val entry: Entry = entryRepository.getEntry(entryId) ?: error("entry missing")
			_screenState.value = EntryDetailScreenState.Loaded(entry)
		}
	}

}
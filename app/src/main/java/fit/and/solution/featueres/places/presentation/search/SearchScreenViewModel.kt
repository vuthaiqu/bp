package fit.and.solution.featueres.places.presentation.search

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fit.and.solution.core.domain.Place
import fit.and.solution.featueres.places.data.PlacesRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

sealed interface SearchScreenState {
	data class Searching(
	 val searchString: String = "",
	 val searchResult: List<Place> = listOf()) : SearchScreenState
}

class SearchScreenViewModel(
 private val placesRepository: PlacesRepository = PlacesRepository()) : ViewModel() {
	private var _searchState = MutableStateFlow(SearchScreenState.Searching())
	val searchState get() = _searchState.asStateFlow()

	fun updateSearchString(new: String) {
		viewModelScope.launch {
			_searchState.value =
				SearchScreenState.Searching(searchResult = if (new != "") placesRepository.getPlaces()
					.filter { it.name.contains(new) }
				else listOf(),
				 searchString = new)
		}
	}
}
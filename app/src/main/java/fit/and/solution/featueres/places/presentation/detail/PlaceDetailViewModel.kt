package fit.and.solution.featueres.places.presentation.detail

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fit.and.solution.core.domain.Place
import fit.and.solution.core.presentation.Screens
import fit.and.solution.featueres.places.data.PlacesRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

sealed interface PlaceDetailScreenState {
	data object Loading : PlaceDetailScreenState
	data class Loaded (val place: Place) : PlaceDetailScreenState
}

class PlaceDetailViewModel(
 private val savedStateHandle: SavedStateHandle,
 private val placesRepository: PlacesRepository = PlacesRepository()) : ViewModel() {
	private val _screenState = MutableStateFlow<PlaceDetailScreenState>(PlaceDetailScreenState.Loading)
	val screenState get() = _screenState.asStateFlow()

	init {
		viewModelScope.launch {

			val placeId: String = savedStateHandle[Screens.PlaceDetail.ID] ?: error("place id missing")
			val place: Place = placesRepository.getPlace(placeId) ?: error("place missing")
			_screenState.value = PlaceDetailScreenState.Loaded(place)

		}
	}

}
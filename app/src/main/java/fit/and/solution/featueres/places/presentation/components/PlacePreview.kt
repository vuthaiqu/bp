package fit.and.solution.featueres.places.presentation.components

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import fit.and.solution.R
import fit.and.solution.core.domain.Place
import fit.and.solution.core.presentation.ui.components.FormatNumber


@Composable
fun PlacePreview(
 headLine: String = "",
 place: Place,
 onClick: () -> Unit,
) {
	Column(modifier = Modifier
		.padding(bottom = 10.dp)
		.clickable { onClick() }) {
		Divider(
		 Modifier.fillMaxWidth(),
		 thickness = 0.5.dp,
		 color = MaterialTheme.colorScheme.onSurface)
		Preview(
		 headLine = headLine,
		 place = place)
		Divider(
		 Modifier.fillMaxWidth(),
		 thickness = 0.5.dp,
		 color = MaterialTheme.colorScheme.onSurface)
	}
}

@Composable
fun VisitingPlace(
 place: Place,
 color: Color,
 navToDetail: () -> Unit,
 addVisited: () -> Unit) {
	Column(modifier = Modifier
		.padding(bottom = 10.dp)
		.background(color)
		.clickable { navToDetail() }) {
		Divider(
		 Modifier.fillMaxWidth(),
		 thickness = 0.5.dp,
		 color = MaterialTheme.colorScheme.onSurface)
		Preview(
		 headLine = "",
		 place = place)
		Row(
		 modifier = Modifier
			 .fillMaxWidth()
			 .clickable { addVisited() }
			 .padding(10.dp),
		 verticalAlignment = Alignment.CenterVertically) {
			Icon(
			 modifier = Modifier.size(30.dp),
			 painter = painterResource(id = R.drawable.add),
			 contentDescription = R.drawable.add.toString())
			Text(
			 text = stringResource(R.string.add_to_journey),
			 fontSize = 25.sp)
		}
		Divider(
		 Modifier.fillMaxWidth(),
		 thickness = 0.5.dp,
		 color = MaterialTheme.colorScheme.onSurface)
	}

}

@Composable
private fun Preview(
 headLine: String,
 place: Place) {
	Column(
	 modifier = Modifier.padding(10.dp)) {
		if (headLine != "") Text(
		 text = headLine,
		 fontSize = 20.sp,
		 fontWeight = FontWeight.Bold)
		Row(
		 modifier = Modifier
			 .fillMaxWidth()
			 .height(180.dp),
		 horizontalArrangement = Arrangement.Start) {
			place.image.ComposeImage(
			 contentScale = ContentScale.Crop,
			 modifier = Modifier
				 .weight(5f)
				 .padding(end = 4.dp)
				 .border(
				  width = 1.dp,
				  color = MaterialTheme.colorScheme.onSurface,
				  shape = RoundedCornerShape(5))
				 .clip(RoundedCornerShape(5)))
			Text(
			 overflow = TextOverflow.Ellipsis,
			 fontSize = 12.sp,
			 modifier = Modifier.weight(3f),
			 text = place.description)
		}
		Row(
		 modifier = Modifier.padding(top = 2.dp),
		 verticalAlignment = Alignment.CenterVertically,
		) {
			Row(
			 verticalAlignment = Alignment.CenterVertically,
			 modifier = Modifier
				 .padding(end = 4.dp)
				 .weight(5f)) {
				Text(
				 maxLines = 1,
				 overflow = TextOverflow.Ellipsis,
				 modifier = Modifier.weight(6f),
				 text = place.name,
				 fontSize = 15.sp)
				Row(
				 modifier = Modifier.weight(4f),
				 horizontalArrangement = Arrangement.End) {
					FormatNumber(
					 modifier = Modifier.padding(end = 5.dp),
					 fontSize = 15.sp,
					 number = place.visits)
					Text(text = "${place.rating}")
					Icon(
					 painter = painterResource(id = R.drawable.star),
					 tint = MaterialTheme.colorScheme.onSurface,
					 contentDescription = "stars",
					 modifier = Modifier.size(18.dp))
				}
			}
			Row(
			 verticalAlignment = Alignment.CenterVertically,
			 modifier = Modifier.weight(3f)) {
				Icon(
				 painter = painterResource(id = place.category.iconRes),
				 contentDescription = stringResource(id = place.category.nameResource),
				 modifier = Modifier.size(18.dp))
				Spacer(modifier = Modifier.weight(1f))
				Icon(
				 painter = painterResource(id = place.recommendedSeason.iconRes),
				 contentDescription = stringResource(id = place.recommendedSeason.nameRes),
				 modifier = Modifier.size(18.dp))
			}
		}
	}
}


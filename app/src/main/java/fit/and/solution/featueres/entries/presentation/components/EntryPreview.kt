package fit.and.solution.featueres.entries.presentation.components

import androidx.compose.runtime.Composable


import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.ModifierLocalBeyondBoundsLayout
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import fit.and.solution.R
import fit.and.solution.featueres.entries.domain.Entry
import java.time.format.DateTimeFormatter

@Composable
fun EntryPreview(
 entry: Entry,
 onClick: () -> Unit,
) {
	Column(modifier = Modifier
		.padding(bottom = 10.dp)
		.clickable { onClick() }) {
		Divider(
		 Modifier.fillMaxWidth(),
		 thickness = 0.5.dp,
		 color = MaterialTheme.colorScheme.onSurface)
		Preview(entry = entry)
		Divider(
		 Modifier.fillMaxWidth(),
		 thickness = 0.5.dp,
		 color = MaterialTheme.colorScheme.onSurface)
	}
}

@Composable
private fun Preview(
 entry: Entry) {
	Column(
	 modifier = Modifier
		 .padding(10.dp)
		 .background(MaterialTheme.colorScheme.surface)) {
		Row(
		 modifier = Modifier
			 .fillMaxWidth()
			 .height(180.dp),
		 horizontalArrangement = Arrangement.Start) {
			entry.image.ComposeImage(
			 contentScale = ContentScale.Crop,
			 modifier = Modifier
				 .weight(5f)
				 .padding(end = 4.dp)
				 .border(
				  width = 1.dp,
				  color = MaterialTheme.colorScheme.onSurface,
				  shape = RoundedCornerShape(5))
				 .clip(RoundedCornerShape(5)))
			Text(
			 overflow = TextOverflow.Ellipsis,
			 fontSize = 12.sp,
			 modifier = Modifier
				 .weight(3f)
				 .padding(end = 4.dp),
			 text = entry.text)
		}
		Row(
		 modifier = Modifier.padding(top = 2.dp),
		 verticalAlignment = Alignment.CenterVertically,
		) {
			Row(
			 modifier = Modifier.weight(5f),
			 verticalAlignment = Alignment.CenterVertically) {
				Text(
				 maxLines = 1,
				 overflow = TextOverflow.Ellipsis,
				 text = entry.place.name,
				 fontSize = 15.sp)
			}
			Row(
			 modifier = Modifier.weight(3f),
			 verticalAlignment = Alignment.CenterVertically) {
				Text(
				 modifier = Modifier.padding(horizontal = 4.dp),
				 text = entry.visitDate.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")),
				 fontSize = 17.sp)
				Icon(
				 modifier = Modifier
					 .size(22.dp)
					 .padding(horizontal = 4.dp),
				 painter = painterResource(id = entry.transportation.iconRes),
				 contentDescription = stringResource(id = entry.transportation.nameResource))
				Icon(
				 modifier = Modifier
					 .size(22.dp)
					 .padding(horizontal = 4.dp),
				 painter = painterResource(id = entry.weather.iconRes),
				 contentDescription = stringResource(id = entry.weather.nameResource))
			}
		}
		Row(
		 modifier = Modifier.padding(top = 2.dp),
		 verticalAlignment = Alignment.CenterVertically,
		) {
			for (i: Int in 1..5) {
				Icon(
				 modifier = Modifier
					 .size(20.dp)
					 .padding(3.dp),
				 painter = painterResource(id = R.drawable.star),
				 contentDescription = R.drawable.star.toString(),
				 tint = if (entry.place.rating >= i) MaterialTheme.colorScheme.secondary else MaterialTheme.colorScheme.onSurface)
			}
		}
	}
}
package fit.and.solution.core.presentation.ui.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ScaffoldTopBar(
 title: String,
 backIcon: @Composable () -> Unit = {},
 additionalActions: @Composable RowScope.() -> Unit = {},
 content: @Composable (PaddingValues) -> Unit) {
	Scaffold(topBar = {
		Column {
			TopAppBar(title = { Text(text = title) },
			 navigationIcon = { backIcon() })
			Divider(Modifier.fillMaxWidth())
		}
	}) {
		content(it)
	}
}

package fit.and.solution.core.presentation

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.calculateEndPadding
import androidx.compose.foundation.layout.calculateStartPadding
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.unit.dp
import androidx.navigation.NavDestination.Companion.hierarchy
import androidx.navigation.NavGraph.Companion.findStartDestination
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import fit.and.solution.core.presentation.ui.components.NavBar
import fit.and.solution.featueres.entries.presentation.detail.EntryDetailScreen
import fit.and.solution.featueres.entries.presentation.list.Entries
import fit.and.solution.featueres.home.presentation.Home
import fit.and.solution.featueres.places.presentation.detail.PlaceDetailScreen
import fit.and.solution.featueres.places.presentation.search.SearchScreen

@Composable
fun Navigation() {
	val navController = rememberNavController()
	val navBackStackEntry by navController.currentBackStackEntryAsState()
	val currentDestination = navBackStackEntry?.destination
	Scaffold(bottomBar = {
		NavBar(select = {
			navController.navigate(it) {
				popUpTo(navController.graph.findStartDestination().id)
				launchSingleTop = true
			}
		},
		 selected = { it == currentDestination?.hierarchy?.first()?.route })
	}) { innerPadding ->
		NavHost(
		 modifier = Modifier.padding(
			top = innerPadding.calculateTopPadding(),
			start = innerPadding.calculateStartPadding(LocalLayoutDirection.current),
			end = innerPadding.calculateEndPadding(LocalLayoutDirection.current),
			bottom = 0.dp),
		 navController = navController,
		 startDestination = Screens.TopLevel.Home.route) {
			val bottomBarPadding: PaddingValues = PaddingValues(
			 top = 0.dp,
			 start = 0.dp,
			 end = 0.dp,
			 bottom = innerPadding.calculateBottomPadding())
			composable(route = Screens.TopLevel.Home.route) {
				Home(paddingValues = bottomBarPadding,
				 navigateToPlaceDetail = { navController.navigate(Screens.PlaceDetail(it).route) },
				 navigateToSearchScreen = { navController.navigate(Screens.Search.route) })
			}
			composable(route = Screens.TopLevel.FollowedTravelers.route) {
				Column {
					Text(text = "prototype not implemented")
				}
			}
			composable(route = Screens.TopLevel.MyJourney.route) {
				Entries(paddingValues = bottomBarPadding) {
					navController.navigate(Screens.EntryDetail(it).route)
				}
			}
			composable(route = Screens.TopLevel.Map.route) {
				Column {
					Text(text = "prototype not implemented")
				}
			}
			composable(route = Screens.TopLevel.List.route) {
				Column {
					Text(text = "prototype not implemented")
				}
			}
			composable(route = Screens.Search.route) {
				SearchScreen(paddingValues = bottomBarPadding,
				 navigateBack = { navController.navigateUp() },
				 navigateToPlaceDetail = { navController.navigate(Screens.PlaceDetail(it).route) })
			}
			composable(
			 route = Screens.PlaceDetail("{${Screens.PlaceDetail.ID}}").route,
			 arguments = listOf(navArgument(Screens.PlaceDetail.ID) { type = NavType.StringType }),
			) {
				PlaceDetailScreen(paddingValues = bottomBarPadding,
				 navigateUp = { navController.navigateUp() })
			}
			composable(
			 route = Screens.EntryDetail("{${Screens.EntryDetail.ID}}").route,
			 arguments = listOf(navArgument(Screens.EntryDetail.ID) { type = NavType.StringType }),
			) {
				EntryDetailScreen(paddingValues = bottomBarPadding,
				 navigateUp = { navController.navigateUp() })
			}

		}
	}
}


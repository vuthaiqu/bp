package fit.and.solution.core.presentation.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import fit.and.solution.R

@Composable
fun SearchBarFrame(
 modifier: Modifier = Modifier,
 content: @Composable RowScope.() -> Unit) {
  Box(
   modifier = modifier
     .padding(10.dp)
     .fillMaxWidth()
     .background(MaterialTheme.colorScheme.surface)
     .border(
      width = 1.dp,
      color = MaterialTheme.colorScheme.primary,
      shape = RoundedCornerShape(50)),
  ) {
    Row(
     horizontalArrangement = Arrangement.Start,
     modifier = Modifier
       .padding(5.dp)) {
      content()
    }
  }
}

@Composable
fun NotActiveSearchBar(
 text: String = stringResource(R.string.journey_starts_with_a_single_search),
 onClick: () -> Unit,
) {
  SearchBarFrame(modifier = Modifier.clickable { onClick() }) {
    Icon(
     painter = painterResource(id = R.drawable.search),
     contentDescription = "search",
     tint = MaterialTheme.colorScheme.onSurface)
    Text(
     text = text,
     fontSize = 20.sp)
  }
}

@Composable
fun ActiveSearchBar(
 searchText: String = "",
 onValueChange: (String) -> Unit,
 onCancel: () -> Unit) {
  SearchBarFrame() {
    BasicTextField(
     singleLine = true,
     modifier = Modifier
       .padding(start = 8.dp)
       .weight(1f),
     textStyle = TextStyle(fontSize = 20.sp),
     value = searchText,
     onValueChange = onValueChange)
    Icon(
     modifier = Modifier
       .clickable { onCancel() },
     tint = MaterialTheme.colorScheme.onSurface,
     painter = painterResource(id = R.drawable.cancel),
     contentDescription = "cancel")
  }
}


package fit.and.solution.core.data

import VisitedPlace
import fit.and.solution.R
import fit.and.solution.core.domain.Category
import fit.and.solution.core.domain.GPSLocation
import fit.and.solution.core.domain.Place
import fit.and.solution.core.domain.RecommendedSeason
import fit.and.solution.core.presentation.ui.image.Image
import fit.and.solution.core.presentation.ui.image.ResourceImage
import fit.and.solution.featueres.entries.domain.Entry
import fit.and.solution.featueres.entries.domain.Transportation
import fit.and.solution.featueres.entries.domain.Weather
import java.time.LocalDate

object Data {
	val places = listOf(
	 Place(
		id = "0",
		location = GPSLocation(
		 x = 50.0,
		 y = 14.3),
		name = "Prague Castle",
		description = "Prague Castle is a castle complex in Prague, Czech Republic serving as the official residence and workplace of the president of the Czech Republic. Built in the 9th century, the castle has long served as the seat of power for kings of Bohemia, Holy Roman emperors, and presidents of Czechoslovakia. As such, the term \"Prague Castle\" or simply the \"Castle\" or \"the Hrad\" are often used as metonymy for the president and his staff and advisors. The Bohemian Crown Jewels are kept within a hidden room inside it.\n" + "\n" + "According to the Guinness Book of Records, Prague Castle is the largest ancient castle in the world, occupying an area of almost 70,000 square metres (750,000 square feet), at about 570 metres (1,870 feet) in length and an average of about 130 metres (430 feet) wide. The castle is among the most visited tourist attractions in Prague, attracting over 1.8 million visitors annually.",
		recommendedSeason = RecommendedSeason.ALL_YEAR,
		category = Category.HISTORICAL_SIGHT,
		image = ResourceImage(R.drawable.prague_castle),
		rating = 4.2,
		visits = 543210,
	 ),
	 Place(
		id = "1",
		location = GPSLocation(
		 x = 22.3,
		 y = 103.8),
		name = "Sa Pa",
		description = "Sa Pa is a district-level town of Lào Cai Province in the Northwest region of Vietnam. As of 2018, the town had a population of 61,498. The town covers an area of 677 km2. The town capital lies at Sa Pa ward. It is one of the main market and touristic towns in the area, where several ethnic minority groups such as Hmong, Dao (Yao), Giáy, Xa Pho, and Tay live.",
		recommendedSeason = RecommendedSeason.SPRING,
		category = Category.NATURE,
		image = ResourceImage(R.drawable.sapa),
		rating = 4.8,
		visits = 5678,
	 ),

	 Place(
		id = "2",
		location = GPSLocation(
		 x = 49.96,
		 y = 14.2),
		name = "Velka Amerika (Big America)",
		description = "Velká Amerika (literally \"Big America\") is a partly flooded, abandoned limestone quarry in the Mořina municipality in the Central Bohemian Region of the Czech Republic. It lies about 15 km (9 mi) southwest of Prague.",
		recommendedSeason = RecommendedSeason.SUMMER,
		category = Category.NATURE,
		image = ResourceImage(R.drawable.big_america),
		rating = 4.8,
		visits = 6,

		)


	)
	val entries = listOf<Entry>(
	 Entry(
		entryId = "0",
		place = places[2],
		text = "It was raining, the road muddy, we also could not get inside, because of some running " + "contest. We could have not picked worse day to visit the place, but the meal THE BOYS " + "prepared bit saved the trip. In the end the view was nice and we plan to visit it in better " + "weather.",
		image = ResourceImage(R.drawable.the_trip),
		rating = 4,
		visitDate = LocalDate.of(
		 2024,
		 4,
		 13),
		weather = Weather.RAINY,
		transportation = Transportation.WALK,
	 ),
	)

	val visiting = mutableListOf(places[0])

	val visited = mutableListOf<VisitedPlace>()
}
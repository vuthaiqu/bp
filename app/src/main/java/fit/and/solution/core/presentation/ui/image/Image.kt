package fit.and.solution.core.presentation.ui.image

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale

interface Image {
    @Composable
    fun ComposeImage(
        modifier: Modifier,
        contentScale: ContentScale
    )
}
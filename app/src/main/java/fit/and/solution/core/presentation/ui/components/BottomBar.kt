package fit.and.solution.core.presentation.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import fit.and.solution.core.presentation.Screens

@Composable
private fun BottomBarPart(
 modifier: Modifier = Modifier,
 content: @Composable () -> Unit) {
	Row(
	 modifier = modifier
		 .height(65.dp)
		 .clickable(enabled = false) {}
		 .background(color = MaterialTheme.colorScheme.primary),
	 verticalAlignment = Alignment.CenterVertically,
	) {
		content()
	}
}

@Composable
private fun NavIcon(
 painter: Painter,
 contentDescription: String,
 modifier: Modifier = Modifier,
 selected: Boolean = false,
 onClick: () -> Unit) {
	Icon(
	 painter = painter,
	 contentDescription = contentDescription,
	 modifier = modifier
		 .padding(10.dp)
		 .fillMaxHeight()
		 .clickable { onClick() },
	 tint = if (selected) MaterialTheme.colorScheme.surfaceVariant else MaterialTheme.colorScheme.secondary)
}

@Composable
fun NavBar(
 select: (route: String) -> Unit,
 selected: (route: String) -> Boolean,
) {
	Row(
	 modifier = Modifier
		 .fillMaxWidth(),
	 verticalAlignment = Alignment.Bottom,
	 horizontalArrangement = Arrangement.Center) {

		BottomBarPart(
		 modifier = Modifier
			 .weight(1f)
			 .clip(RoundedCornerShape(topStart = 12.dp))) {
			Spacer(modifier = Modifier.weight(1f))
			NavIcon(
			 painter = painterResource(id = (Screens.TopLevel.FollowedTravelers.iconResource)),
			 contentDescription = stringResource(id = Screens.TopLevel.FollowedTravelers.titleResource),
			 selected = selected(Screens.TopLevel.FollowedTravelers.route)) {
				select(Screens.TopLevel.FollowedTravelers.route)
			}
			NavIcon(
			 painter = painterResource(id = (Screens.TopLevel.MyJourney.iconResource)),
			 contentDescription = stringResource(id = Screens.TopLevel.MyJourney.iconResource),
			 selected = selected(Screens.TopLevel.MyJourney.route)) {
				select(Screens.TopLevel.MyJourney.route)
			}
		}
		Box(
		 modifier = Modifier
			 .clickable(enabled = false) {}
			 .size(
				width = 90.dp,
				height = 90.dp)
			 .clip(RoundedCornerShape(topEnd = 25.dp))
			 .clip(RoundedCornerShape(topStart = 25.dp))
			 .background(MaterialTheme.colorScheme.primary)) {
			NavIcon(
			 painter = painterResource(id = (Screens.TopLevel.Home.iconResource)),
			 contentDescription = stringResource(id = Screens.TopLevel.Home.iconResource),
			 selected = selected(Screens.TopLevel.Home.route),
			 modifier = Modifier.fillMaxSize()) {
				select(Screens.TopLevel.Home.route)
			}
		}
		BottomBarPart(
		 modifier = Modifier
			 .weight(1f)
			 .clip(RoundedCornerShape(topEnd = 12.dp))) {
			NavIcon(
			 painter = painterResource(id = (Screens.TopLevel.Map.iconResource)),
			 contentDescription = stringResource(id = Screens.TopLevel.Map.iconResource),
			 selected = selected(Screens.TopLevel.Map.route)) {
				select(Screens.TopLevel.Map.route)
			}
			NavIcon(
			 painter = painterResource(id = (Screens.TopLevel.List.iconResource)),
			 contentDescription = stringResource(id = Screens.TopLevel.List.iconResource),
			 selected = selected(Screens.TopLevel.List.route)) {
				select(Screens.TopLevel.List.route)
			}
		}
	}
}
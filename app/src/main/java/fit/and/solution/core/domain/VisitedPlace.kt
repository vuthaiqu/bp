import fit.and.solution.core.domain.Place
import java.time.LocalDate

data class VisitedPlace(
 val place: Place,
 val date: LocalDate
)
package fit.and.solution.core.presentation

import fit.and.solution.R


sealed class Screens(val route: String) {
	sealed class TopLevel(route: String) : Screens(route) {
		abstract val titleResource: Int
		abstract val iconResource: Int

		data object Home : TopLevel("home") {
			override val titleResource = R.string.home
			override val iconResource = R.drawable.home
		}


		data object MyJourney : TopLevel("myJourney") {
			override val titleResource = R.string.my_journey
			override val iconResource = R.drawable.my_journey

		}

		data object FollowedTravelers : TopLevel("followedTravelers") {
			override val titleResource: Int = R.string.followed_travelers
			override val iconResource: Int = R.drawable.followed_travelers
		}


		data object Map : TopLevel("map") {
			override val titleResource: Int = R.string.map
			override val iconResource: Int = R.drawable.map
		}

		data object List : TopLevel("list") {
			override val titleResource: Int = R.string.list
			override val iconResource: Int = R.drawable.list
		}
	}

	data object Search : Screens("search")

	class PlaceDetail(id: String) : Screens("place_detail/$id") {
		companion object {
			const val ID  = "id"
		}
	}

	class EntryDetail(id: String) : Screens("entry_detail/$id") {
		companion object {
			const val ID  = "id"
		}
	}
}
package fit.and.solution.core.presentation.ui.components

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.TextUnit
import fit.and.solution.R
import kotlin.math.pow
import kotlin.math.roundToInt

@Composable
fun FormatNumber(
 modifier: Modifier = Modifier,
 fontSize: TextUnit,
 number: Int) {
	Text(
	 fontSize = fontSize,
	 modifier = modifier,
	 text = if (number >= 10f.pow(9)) {
		 "${(number / 10f.pow(9)).roundToInt()} " + stringResource(R.string.billions)
	 } else if (number >= 10f.pow(6)) {
		 "${(number / 10f.pow(6)).roundToInt()} " + stringResource(R.string.millions)
	 } else if (number >= 10f.pow(3)) {
		 "${(number / 10f.pow(3)).roundToInt()} " + stringResource(R.string.thousand)
	 } else "$number")
}
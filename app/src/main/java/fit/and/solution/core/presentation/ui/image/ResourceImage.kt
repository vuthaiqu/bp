package fit.and.solution.core.presentation.ui.image

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource

class ResourceImage (private val id: Int) : Image {
    @Composable
    override fun ComposeImage(
        modifier: Modifier,
        contentScale: ContentScale) {
        Image(
            modifier = modifier.fillMaxSize(),
            painter = painterResource(id = id),
            contentScale = contentScale,
            contentDescription = null)
    }
}
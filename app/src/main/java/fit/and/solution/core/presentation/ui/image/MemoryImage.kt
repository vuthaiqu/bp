package fit.and.solution.core.presentation.ui.image

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.imageResource
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.compose.rememberAsyncImagePainter
import coil.compose.rememberImagePainter
import coil.request.ImageRequest
import java.io.File


class MemoryImage (private val path: String) : Image {
    @Composable
    override fun ComposeImage(
        modifier: Modifier,
        contentScale: ContentScale) {
        AsyncImage(
            model = ImageRequest.Builder(LocalContext.current).data(path).build(),
            contentDescription = "icon",
            contentScale = contentScale,
            modifier = modifier.fillMaxSize())
    }
}
package fit.and.solution.core.presentation.ui.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import fit.and.solution.R

@Composable
fun DetailTemplate(
 paddingValues: PaddingValues,
 title: String,
 navigateUp: () -> Unit,
 additionalActions: @Composable RowScope.() -> Unit = {},
 content: @Composable ColumnScope.() -> Unit) {
	ScaffoldTopBar(
	 title = title,
	 additionalActions = additionalActions,
	 backIcon = {
		 IconButton(onClick = navigateUp) {
			 Icon(
				painter = painterResource(id = R.drawable.back),
				contentDescription = R.drawable.back.toString())
		 }
	 },
	) {
		val scrollState = rememberScrollState()
		Column(
		 Modifier
			 .padding(it)
			 .verticalScroll(scrollState)
			 .padding(paddingValues)) {
			content()
		}
	}
}


package fit.and.solution.core.domain

import fit.and.solution.R
import fit.and.solution.core.presentation.ui.image.Image

data class Place(
    val id: String,
    val location: GPSLocation,
    val name: String,
    val description: String,
    val recommendedSeason: RecommendedSeason,
    val category: Category,
    val image: Image,
    val rating: Double,
    val visits: Int,
)


data class GPSLocation(val x: Double, val y: Double)

enum class RecommendedSeason(val nameRes : Int, val iconRes: Int){
    SPRING(nameRes = R.string.spring, iconRes = R.drawable.spring),
    SUMMER(nameRes = R.string.summer, iconRes = R.drawable.summer),
    FALL(nameRes = R.string.fall, iconRes = R.drawable.fall),
    WINTER(nameRes = R.string.winter, iconRes = R.drawable.winter),
    ALL_YEAR(nameRes = R.string.all_year, iconRes = R.drawable.all_year)
}

enum class Category(val nameResource: Int = R.string.unnamed_category, val iconRes: Int = R.drawable.history){
    HISTORICAL_SIGHT(nameResource = R.string.historical_sight, iconRes = R.drawable.history),
    NATURE (nameResource = R.string.nature),
    TECH_WORK (nameResource = R.string.technical_work),
    ART_WORK (nameResource = R.string.art_work),
    MUSEUMS(nameResource = R.string.skanzens_and_museums),
    MEMORIALS_MAUSOLEUM (nameResource = R.string.memorials_and_mausoleum),
    RELIGIOUS_SIGHT (nameResource = R.string.religious_sight)
}
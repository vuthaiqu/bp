package fit.and.solution.core.presentation.ui.image

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import coil.compose.AsyncImage

class WebImage(private val url: String): Image {
  @Composable
  override fun ComposeImage(
    modifier: Modifier,
    contentScale: ContentScale) {
    AsyncImage(
      model = url,
      contentDescription = url,
      contentScale = contentScale,
      modifier = modifier.fillMaxSize())
  }

}
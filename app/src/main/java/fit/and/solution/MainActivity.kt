package fit.and.solution

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import fit.and.solution.core.presentation.ui.theme.SolutionTheme
import fit.and.solution.core.presentation.Navigation

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            SolutionTheme {
                Navigation()
            }
        }
    }
}

